### 点狮-OA

 **基于[RuoYi-VUE](https://gitee.com/y_project/RuoYi-Vue)版本开发。** 
 <br/>
 <br/>
1、使用RuoYi-Vue的基础上开发。<br/>
2、集成flowable，并与系统用户角色进行绑定，可非常方便的设置任务的办理人与办理角色。<br/>
3、实现自定义表单，可以托拉拽的方式创建个性化表单。并可自由选择提交的流程<br/>

<br/>

演示地址：<a href='http://admin.dianshixinxi.com:90' target="_blank" >http://admin.dianshixinxi.com:90</a>
 <br/>
 <br/>
 

联系我们：yigexiaochengxuyuan
<br/>
<br/>
![作者微信](PointLion/src/main/resources/fonts/image.png)
<br/>
### 系统特色

1.这里是列表文本前端采用Vue、Element UI。
<br/>
2.后端采用Spring Boot、Spring Security、Redis & Jwt。
<br/>
3.权限认证使用Jwt，支持多终端认证系统。
<br/>
4.支持加载动态权限菜单，多方式轻松权限控制。
<br/>
5.高效率开发，使用代码生成器可以一键生成前后端代码。
<br/>
6.集成Flowable流程引擎
<br/>
7.流程图管理与若依系统用户和角色进行了绑定，可非常方便的设置任务的审批人审批角色
<br/>
8.支持自定义表单，方便业务功能扩展，可将自定义的表单提交任意流程
<br/>
<br/>
<br/>

### 系统功能
1.用户管理：用户是系统操作者，该功能主要完成系统用户配置。
<br/>
2.部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
<br/>
3.岗位管理：配置系统用户所属担任职务。
<br/>
4.菜单管理：配置系统菜单，操作权限，按钮权限标识等。
<br/>
5.角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
<br/>
6.字典管理：对系统中经常使用的一些较为固定的数据进行维护。
<br/>
7.参数管理：对系统动态配置常用参数。
<br/>
8.通知公告：系统通知公告信息发布维护。
<br/>
9.操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
<br/>
10.登录日志：系统登录日志记录查询包含登录异常。
<br/>
11.在线用户：当前系统中活跃用户状态监控。
<br/>
12.定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
<br/>
13.代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
<br/>
14.系统接口：根据业务代码自动生成相关的api接口文档。
<br/>
15.服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
<br/>
16.缓存监控：对系统的缓存信息查询，命令统计等。
<br/>
17.在线构建器：拖动表单元素生成相应的HTML代码。
<br/>
18.连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。
<br/>


### 办公管理功能
1.我的待办：当前登录用户，办理任务
<br/>
2.通知公告：通知与公告，已发布通知公告会显示在首页
<br/>
3.自定义表单：自定义表单，托拉拽的形式，创建及展示用户个性化的表单
<br/>
4.自定义申请：使用自定义的表单，进行流程审批
<br/>
5.运行中的流程：目前系统正在运行的流程
<br/>
6.流程图：流程编辑，可方便的设置审批人与审批角色。以及会签，多实例，子流程，并行审批，串行审批等等
<br/>
7.酒店申请：酒店预订申请
<br/>
8.会议室申请：会议室使用申请
<br/>
9.公章申请：公章使用申请
<br/>
10.车票申请：车船票申请
<br/>
11.车辆申请：车辆使用申请
<br/>
12.报销申请：报销单，报销申请
<br/>
13.差旅费报销申请：出差后差旅费报销申请
<br/>
14.请假申请：请假申请
<br/>
15.加班申请：加班申请
<br/>
16.调休申请：调休申请
<br/>
17.出差申请：出差申请
<br/>
18.外出申请：外出申请
<br/>
19.员工入职登记：员工入职登记
<br/>
20.转正申请：转正申请
<br/>
21.离职申请：离职申请
<br/>
22.车辆管理：车辆管理，字典功能
<br/>
23.会议室管理：会议室管理，字典功能
<br/>
24.公章管理：公章管理，字典功能
<br/>
25.合同协议模版：合同补充协议的模版
<br/>
26.合同补充协议：合同补充协议
<br/>
<br/>
以及其他各种字典维护
<br/>
<br/>

### 首页
![输入图片说明](PointLion/src/main/resources/fonts/1685933089203.jpg)

### 首页通知
![输入图片说明](PointLion/src/main/resources/fonts/%E9%A6%96%E9%A1%B5%E9%80%9A%E7%9F%A5.png)

### 我的待办
![输入图片说明](PointLion/src/main/resources/fonts/%E5%BE%85%E5%8A%9E.png)

### 流程信息展示
![输入图片说明](PointLion/src/main/resources/fonts/%E6%B5%81%E7%A8%8B%E4%BF%A1%E6%81%AF%E5%B1%95%E7%A4%BA.png)

### 办理任务
![输入图片说明](PointLion/src/main/resources/fonts/%E5%8A%9E%E7%90%86%E4%BB%BB%E5%8A%A1.png)

### 自定义表单
![输入图片说明](PointLion/src/main/resources/fonts/%E8%87%AA%E5%AE%9A%E4%B9%89%E8%A1%A8%E5%8D%95.png)

### 自定义表单设计器
![输入图片说明](PointLion/src/main/resources/fonts/%E8%87%AA%E5%AE%9A%E4%B9%89%E8%A1%A8%E5%8D%95%E8%AE%BE%E8%AE%A1%E5%99%A8.png)

### 自定义表单申请
![输入图片说明](PointLion/src/main/resources/fonts/%E8%87%AA%E5%AE%9A%E4%B9%89%E8%A1%A8%E5%8D%95%E7%94%B3%E8%AF%B7.png)

### 运行中的流程管理
![输入图片说明](PointLion/src/main/resources/fonts/%E8%BF%90%E8%A1%8C%E4%B8%AD%E7%9A%84%E6%B5%81%E7%A8%8B%E7%AE%A1%E7%90%86.png)

### 流程图管理
![输入图片说明](PointLion/src/main/resources/fonts/%E6%B5%81%E7%A8%8B%E5%9B%BE%E7%AE%A1%E7%90%86.png)

### 流程图设计器
![输入图片说明](PointLion/src/main/resources/fonts/%E6%B5%81%E7%A8%8B%E5%9B%BE%E8%AE%BE%E8%AE%A1%E5%99%A8.png)

### 设置审批人（或者审批角色）
![输入图片说明](PointLion/src/main/resources/fonts/%E8%AE%BE%E7%BD%AE%E5%AE%A1%E6%89%B9%E4%BA%BA.png)

### 酒店申请（等）
![输入图片说明](PointLion/src/main/resources/fonts/%E9%85%92%E5%BA%97%E7%94%B3%E8%AF%B7.png)

### 报销申请
![输入图片说明](PointLion/src/main/resources/fonts/%E6%8A%A5%E9%94%80%E7%94%B3%E8%AF%B7.png)

### 假勤申请
![输入图片说明](PointLion/src/main/resources/fonts/%E5%81%87%E5%8B%A4%E7%94%B3%E8%AF%B7.png)

### 员工入职登记
![输入图片说明](PointLion/src/main/resources/fonts/%E5%91%98%E5%B7%A5%E5%85%A5%E8%81%8C%E7%99%BB%E8%AE%B0.png)
![输入图片说明](PointLion/src/main/resources/fonts/%E5%85%A5%E8%81%8C%E7%99%BB%E8%AE%B0%E5%8D%A1%E7%89%87.png)

### 补充协议模版
![输入图片说明](PointLion/src/main/resources/fonts/%E5%90%88%E5%90%8C%E8%A1%A5%E5%85%85%E5%8D%8F%E8%AE%AE%E6%A8%A1%E7%89%88.png)

### 合同补充协议
![输入图片说明](PointLion/src/main/resources/fonts/%E8%A1%A5%E5%85%85%E5%8D%8F%E8%AE%AE.png)

<br/>
<br/>
<br/>