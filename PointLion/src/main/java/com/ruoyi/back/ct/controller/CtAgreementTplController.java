package com.ruoyi.back.ct.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.back.ct.domain.CtAgreementTpl;
import com.ruoyi.back.ct.service.CtAgreementTplService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 协议模版Controller
 * 
 * @author pointLion
 * @date 2023-05-13
 */
@RestController
@RequestMapping("/ct/agreementTpl")
public class CtAgreementTplController extends BaseController
{
    @Autowired
    private CtAgreementTplService ctAgreementTplService;

    /**
     * 查询协议模版列表
     */
    @PreAuthorize("@ss.hasPermi('ct:agreementTpl:list')")
    @GetMapping("/list")
    public TableDataInfo list(CtAgreementTpl ctAgreementTpl)
    {
        startPage();
        List<CtAgreementTpl> list = ctAgreementTplService.selectCtAgreementTplList(ctAgreementTpl);
        return getDataTable(list);
    }

    /**
     * 导出协议模版列表
     */
    @PreAuthorize("@ss.hasPermi('ct:agreementTpl:export')")
    @Log(title = "协议模版", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CtAgreementTpl ctAgreementTpl)
    {
        List<CtAgreementTpl> list = ctAgreementTplService.selectCtAgreementTplList(ctAgreementTpl);
        ExcelUtil<CtAgreementTpl> util = new ExcelUtil<CtAgreementTpl>(CtAgreementTpl.class);
        util.exportExcel(response, list, "协议模版数据");
    }

    /**
     * 获取协议模版详细信息
     */
    @PreAuthorize("@ss.hasPermi('ct:agreementTpl:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(ctAgreementTplService.selectCtAgreementTplById(id));
    }

    /**
     * 新增协议模版
     */
    @PreAuthorize("@ss.hasPermi('ct:agreementTpl:add')")
    @Log(title = "协议模版", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CtAgreementTpl ctAgreementTpl)
    {
        return toAjax(ctAgreementTplService.insertCtAgreementTpl(ctAgreementTpl));
    }

    /**
     * 修改协议模版
     */
    @PreAuthorize("@ss.hasPermi('ct:agreementTpl:edit')")
    @Log(title = "协议模版", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CtAgreementTpl ctAgreementTpl)
    {
        return toAjax(ctAgreementTplService.updateCtAgreementTpl(ctAgreementTpl));
    }

    /**
     * 删除协议模版
     */
    @PreAuthorize("@ss.hasPermi('ct:agreementTpl:remove')")
    @Log(title = "协议模版", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(ctAgreementTplService.deleteCtAgreementTplByIds(ids));
    }
}
