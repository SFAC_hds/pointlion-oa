package com.ruoyi.back.ct.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.back.ct.mapper.CtAgreementTplMapper;
import com.ruoyi.back.ct.domain.CtAgreementTpl;

/**
 * 协议模版Service业务层处理
 * 
 * @author pointLion
 * @date 2023-05-13
 */
@Service
public class CtAgreementTplService
{
    @Autowired
    private CtAgreementTplMapper ctAgreementTplMapper;

    /**
     * 查询协议模版
     * 
     * @param id 协议模版主键
     * @return 协议模版
     */
    public CtAgreementTpl selectCtAgreementTplById(Long id)
    {
        return ctAgreementTplMapper.selectCtAgreementTplById(id);
    }

    /**
     * 查询协议模版列表
     * 
     * @param ctAgreementTpl 协议模版
     * @return 协议模版
     */
    public List<CtAgreementTpl> selectCtAgreementTplList(CtAgreementTpl ctAgreementTpl)
    {
        return ctAgreementTplMapper.selectCtAgreementTplList(ctAgreementTpl);
    }

    /**
     * 新增协议模版
     * 
     * @param ctAgreementTpl 协议模版
     * @return 结果
     */
    public int insertCtAgreementTpl(CtAgreementTpl ctAgreementTpl)
    {
        ctAgreementTpl.setCreateTime(DateUtils.getNowDate());
        return ctAgreementTplMapper.insertCtAgreementTpl(ctAgreementTpl);
    }

    /**
     * 修改协议模版
     * 
     * @param ctAgreementTpl 协议模版
     * @return 结果
     */
    public int updateCtAgreementTpl(CtAgreementTpl ctAgreementTpl)
    {
        ctAgreementTpl.setUpdateTime(DateUtils.getNowDate());
        return ctAgreementTplMapper.updateCtAgreementTpl(ctAgreementTpl);
    }

    /**
     * 批量删除协议模版
     * 
     * @param ids 需要删除的协议模版主键
     * @return 结果
     */
    public int deleteCtAgreementTplByIds(Long[] ids)
    {
        return ctAgreementTplMapper.deleteCtAgreementTplByIds(ids);
    }

    /**
     * 删除协议模版信息
     * 
     * @param id 协议模版主键
     * @return 结果
     */
    public int deleteCtAgreementTplById(Long id)
    {
        return ctAgreementTplMapper.deleteCtAgreementTplById(id);
    }
}
