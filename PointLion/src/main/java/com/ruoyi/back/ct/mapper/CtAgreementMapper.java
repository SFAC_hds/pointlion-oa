package com.ruoyi.back.ct.mapper;

import java.util.List;
import com.ruoyi.back.ct.domain.CtAgreement;

/**
 * 补充协议Mapper接口
 * 
 * @author pointLion
 * @date 2023-05-13
 */
public interface CtAgreementMapper 
{
    /**
     * 查询补充协议
     * 
     * @param id 补充协议主键
     * @return 补充协议
     */
    public CtAgreement selectCtAgreementById(Long id);

    /**
     * 查询补充协议列表
     * 
     * @param ctAgreement 补充协议
     * @return 补充协议集合
     */
    public List<CtAgreement> selectCtAgreementList(CtAgreement ctAgreement);

    /**
     * 新增补充协议
     * 
     * @param ctAgreement 补充协议
     * @return 结果
     */
    public int insertCtAgreement(CtAgreement ctAgreement);

    /**
     * 修改补充协议
     * 
     * @param ctAgreement 补充协议
     * @return 结果
     */
    public int updateCtAgreement(CtAgreement ctAgreement);

    /**
     * 删除补充协议
     * 
     * @param id 补充协议主键
     * @return 结果
     */
    public int deleteCtAgreementById(Long id);

    /**
     * 批量删除补充协议
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCtAgreementByIds(Long[] ids);
}
