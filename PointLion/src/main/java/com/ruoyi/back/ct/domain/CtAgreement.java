package com.ruoyi.back.ct.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 补充协议对象 ct_agreement
 * 
 * @author pointLion
 * @date 2023-05-13
 */
@Data
public class CtAgreement extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 合同编号 */
    @Excel(name = "合同编号")
    private String contractNo;

    /** 协议名称 */
    @Excel(name = "协议名称")
    private String contractName;

    /** 签约地点 */
    @Excel(name = "签约地点")
    private String signAddr;

    /** 最低保证金 */
    @Excel(name = "最低保证金")
    private BigDecimal lowBond;

    /** 挂账方式 */
    @Excel(name = "挂账方式")
    private String gzType;

    /** 支付方式 */
    @Excel(name = "支付方式")
    private String payType;

    /** 应付款 */
    @Excel(name = "应付款")
    private BigDecimal needCharge;

    /** 预付款 */
    @Excel(name = "预付款")
    private BigDecimal advanceCharge;

    /** 协议价格 */
    @Excel(name = "协议价格")
    private BigDecimal agreementAmt;

    /** 材质 */
    @Excel(name = "材质")
    private String material;

    /** 参考价 */
    @Excel(name = "参考价")
    private BigDecimal referencePrice;

    /** 定价基准 */
    @Excel(name = "定价基准")
    private String pricingBenchmark;

    /** 补充 */
    @Excel(name = "补充")
    private String other;

    /** 甲方编码 */
    @Excel(name = "甲方编码")
    private String orgNo;

    /** 甲方名称 */
    @Excel(name = "甲方名称")
    private String orgName;

    /** 甲方法人 */
    @Excel(name = "甲方法人")
    private String orgFr;

    /** 甲方地址 */
    @Excel(name = "甲方地址")
    private String orgAddr;

    /** 甲方委托代理人 */
    @Excel(name = "甲方委托代理人")
    private String orgWtr;

    /** 甲方电话 */
    @Excel(name = "甲方电话")
    private String orgTel;

    /** 甲方签证 */
    @Excel(name = "甲方签证")
    private String okDt;

    /** 乙方编码 */
    @Excel(name = "乙方编码")
    private String supplyNo;

    /** 乙方名称 */
    @Excel(name = "乙方名称")
    private String supplyName;

    /** 乙方法人 */
    @Excel(name = "乙方法人")
    private String spFr;

    /** 乙方地址 */
    @Excel(name = "乙方地址")
    private String spAddr;

    /** 乙方委托人 */
    @Excel(name = "乙方委托人")
    private String spWtr;

    /** 乙方电话 */
    @Excel(name = "乙方电话")
    private String spTel;

    /** 乙方签证 */
    private String spOkDt;



    /** 申请流程 */
    @Excel(name = "申请流程")
    private String flowKey;

    /** 流程名称 */
    @Excel(name = "流程名称")
    private String flowName;

    /** 流程实例id */
    @Excel(name = "流程实例id")
    private String flowInsId;


    /** 状态 */
    @Excel(name = "状态", readConverterExp = "0=未提交,1=已提交,2=已完成")
    private String status;

    /** 提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "提交时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date submitTime;


    private Long tplId;

    private String tplName;

    private String url;

    private Date byTime;

    //是否预警
    private Integer ifNotice;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("contractNo", getContractNo())
            .append("contractName", getContractName())
            .append("signAddr", getSignAddr())
            .append("lowBond", getLowBond())
            .append("gzType", getGzType())
            .append("payType", getPayType())
            .append("needCharge", getNeedCharge())
            .append("advanceCharge", getAdvanceCharge())
            .append("agreementAmt", getAgreementAmt())
            .append("material", getMaterial())
            .append("referencePrice", getReferencePrice())
            .append("pricingBenchmark", getPricingBenchmark())
            .append("other", getOther())
            .append("orgNo", getOrgNo())
            .append("orgName", getOrgName())
            .append("orgFr", getOrgFr())
            .append("orgAddr", getOrgAddr())
            .append("orgWtr", getOrgWtr())
            .append("orgTel", getOrgTel())
            .append("okDt", getOkDt())
            .append("supplyNo", getSupplyNo())
            .append("supplyName", getSupplyName())
            .append("spFr", getSpFr())
            .append("spAddr", getSpAddr())
            .append("spWtr", getSpWtr())
            .append("spTel", getSpTel())
            .append("spOkDt", getSpOkDt())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
