package com.ruoyi.back.ct.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.back.ct.domain.CtAgreement;
import com.ruoyi.back.ct.service.CtAgreementService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 补充协议Controller
 * 
 * @author pointLion
 * @date 2023-05-13
 */
@RestController
@RequestMapping("/ct/agreement")
public class CtAgreementController extends BaseController
{
    @Autowired
    private CtAgreementService ctAgreementService;

    /**
     * 查询补充协议列表
     */
    @PreAuthorize("@ss.hasPermi('ct:agreement:list')")
    @GetMapping("/list")
    public TableDataInfo list(CtAgreement ctAgreement)
    {
        startPage();
        List<CtAgreement> list = ctAgreementService.selectCtAgreementList(ctAgreement);
        return getDataTable(list);
    }

    /**
     * 导出补充协议列表
     */
    @PreAuthorize("@ss.hasPermi('ct:agreement:export')")
    @Log(title = "补充协议", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CtAgreement ctAgreement)
    {
        List<CtAgreement> list = ctAgreementService.selectCtAgreementList(ctAgreement);
        ExcelUtil<CtAgreement> util = new ExcelUtil<CtAgreement>(CtAgreement.class);
        util.exportExcel(response, list, "补充协议数据");
    }

    /**
     * 获取补充协议详细信息
     */
    @PreAuthorize("@ss.hasPermi('ct:agreement:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(ctAgreementService.selectCtAgreementById(id));
    }

    /**
     * 新增补充协议
     */
    @PreAuthorize("@ss.hasPermi('ct:agreement:add')")
    @Log(title = "补充协议", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CtAgreement ctAgreement)
    {
        ctAgreement.setCreateBy(getLoginUser().getUsername());
        return toAjax(ctAgreementService.insertCtAgreement(ctAgreement));
    }

    /**
     * 修改补充协议
     */
    @PreAuthorize("@ss.hasPermi('ct:agreement:edit')")
    @Log(title = "补充协议", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CtAgreement ctAgreement)
    {
        ctAgreement.setUpdateBy(getLoginUser().getUsername());
        return toAjax(ctAgreementService.updateCtAgreement(ctAgreement));
    }

    /**
     * 删除补充协议
     */
    @PreAuthorize("@ss.hasPermi('ct:agreement:remove')")
    @Log(title = "补充协议", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(ctAgreementService.deleteCtAgreementByIds(ids));
    }


    /*****
     * 预览
     * @param id
     * @return
     */
    @RequestMapping("/previewAgreemwnt")
    public AjaxResult previewAgreemwnt(@RequestParam(name = "id")Long id){
        String content = ctAgreementService.buildAgreementContent(id);
        return success("成功",content);
    }

    /****
     * 生成文件
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/buildFile/{id}")
    public AjaxResult buildFile(@PathVariable("id")Long id) throws Exception {
        ctAgreementService.buildFile(id);
        return success();
    }


    /*****
     * 提交流程
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/submitFlow/{id}")
    public AjaxResult submitFlow(@PathVariable("id")Long id) throws Exception {
        ctAgreementService.submitFlow(id);
        return success();
    }


    /*****
     * 取回流程
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/cancleFlow/{id}")
    public AjaxResult cancleFlow(@PathVariable("id")Long id) throws Exception {
        ctAgreementService.cancleFlow(id);
        return success();
    }
}
