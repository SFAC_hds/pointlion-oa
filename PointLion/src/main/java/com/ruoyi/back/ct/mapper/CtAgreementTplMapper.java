package com.ruoyi.back.ct.mapper;

import java.util.List;
import com.ruoyi.back.ct.domain.CtAgreementTpl;

/**
 * 协议模版Mapper接口
 * 
 * @author pointLion
 * @date 2023-05-13
 */
public interface CtAgreementTplMapper 
{
    /**
     * 查询协议模版
     * 
     * @param id 协议模版主键
     * @return 协议模版
     */
    public CtAgreementTpl selectCtAgreementTplById(Long id);

    /**
     * 查询协议模版列表
     * 
     * @param ctAgreementTpl 协议模版
     * @return 协议模版集合
     */
    public List<CtAgreementTpl> selectCtAgreementTplList(CtAgreementTpl ctAgreementTpl);

    /**
     * 新增协议模版
     * 
     * @param ctAgreementTpl 协议模版
     * @return 结果
     */
    public int insertCtAgreementTpl(CtAgreementTpl ctAgreementTpl);

    /**
     * 修改协议模版
     * 
     * @param ctAgreementTpl 协议模版
     * @return 结果
     */
    public int updateCtAgreementTpl(CtAgreementTpl ctAgreementTpl);

    /**
     * 删除协议模版
     * 
     * @param id 协议模版主键
     * @return 结果
     */
    public int deleteCtAgreementTplById(Long id);

    /**
     * 批量删除协议模版
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCtAgreementTplByIds(Long[] ids);
}
