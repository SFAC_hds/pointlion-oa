package com.ruoyi.back.home.controller;

import com.itextpdf.text.BaseColor;
import com.ruoyi.back.home.service.HomeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/***
 * @des
 * @author Ly
 * @date 2023/6/4
 */
@RestController
public class PointLionHomeController extends BaseController {

    @Autowired
    private HomeService homeService;

    /****
     * 首页数量统计
     * @return
     */
    @GetMapping("/home/getItemCountData")
    public AjaxResult getItemCountData(){
        Map<String,Long> map = homeService.getItemCountData();
        return success(map);
    }
}
