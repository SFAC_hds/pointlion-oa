package com.ruoyi.back.oa.controller;

import com.ruoyi.back.oa.service.FlowProcessInstanceService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/***
 * @des
 * @author Ly
 * @date 2023/5/31
 */
@RestController
@RequestMapping("/oa/processInstance")
public class ActProcessInsController extends BaseController {

    @Autowired
    private FlowProcessInstanceService flowProcessInstanceService;

    @RequestMapping("/deleteAllProcessInstance")
    public AjaxResult deleteAllProcessInstance(){
        flowProcessInstanceService.deleteAllProcessInstance();
        return success();
    }
}
