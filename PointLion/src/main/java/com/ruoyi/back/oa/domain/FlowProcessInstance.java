package com.ruoyi.back.oa.domain;

import lombok.Data;

/***
 * @des
 * @author Ly
 * @date 2023/6/2
 */
@Data
public class FlowProcessInstance {

    private String billType;

    private String businessId;



}
