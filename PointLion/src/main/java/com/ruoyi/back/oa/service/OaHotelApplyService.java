package com.ruoyi.back.oa.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.back.oa.mapper.OaHotelApplyMapper;
import com.ruoyi.back.oa.domain.OaHotelApply;
import com.ruoyi.back.oa.service.OaHotelApplyService;

/**
 * 酒店申请Service业务层处理
 * 
 * @author pointLion
 * @date 2023-05-29
 */
@Service
public class OaHotelApplyService
{
    @Autowired
    private OaHotelApplyMapper oaHotelApplyMapper;

    /**
     * 查询酒店申请
     * 
     * @param id 酒店申请主键
     * @return 酒店申请
     */
    public OaHotelApply selectOaHotelApplyById(Long id)
    {
        return oaHotelApplyMapper.selectOaHotelApplyById(id);
    }

    /**
     * 查询酒店申请列表
     * 
     * @param oaHotelApply 酒店申请
     * @return 酒店申请
     */
    public List<OaHotelApply> selectOaHotelApplyList(OaHotelApply oaHotelApply)
    {
        return oaHotelApplyMapper.selectOaHotelApplyList(oaHotelApply);
    }

    /**
     * 新增酒店申请
     * 
     * @param oaHotelApply 酒店申请
     * @return 结果
     */
    public int insertOaHotelApply(OaHotelApply oaHotelApply)
    {
        oaHotelApply.setCreateTime(DateUtils.getNowDate());
        return oaHotelApplyMapper.insertOaHotelApply(oaHotelApply);
    }

    /**
     * 修改酒店申请
     * 
     * @param oaHotelApply 酒店申请
     * @return 结果
     */
    public int updateOaHotelApply(OaHotelApply oaHotelApply)
    {
        oaHotelApply.setUpdateTime(DateUtils.getNowDate());
        return oaHotelApplyMapper.updateOaHotelApply(oaHotelApply);
    }

    /**
     * 批量删除酒店申请
     * 
     * @param ids 需要删除的酒店申请主键
     * @return 结果
     */
    public int deleteOaHotelApplyByIds(Long[] ids)
    {
        return oaHotelApplyMapper.deleteOaHotelApplyByIds(ids);
    }

    /**
     * 删除酒店申请信息
     * 
     * @param id 酒店申请主键
     * @return 结果
     */
    public int deleteOaHotelApplyById(Long id)
    {
        return oaHotelApplyMapper.deleteOaHotelApplyById(id);
    }
}
