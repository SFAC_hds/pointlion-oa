package com.ruoyi.back.oa.mapper;

import com.ruoyi.back.oa.domain.VTasklist;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 流程实例
 * 
 * @author pointLion
 * @date 2023-05-31
 */
public interface FlowProcessMapper
{
    public void updateBusinessBillStatus(@Param("tableName") String tableName,
                                         @Param("status") String status,
                                         @Param("insId") String insId,
                                         @Param("id") Long id);
}
