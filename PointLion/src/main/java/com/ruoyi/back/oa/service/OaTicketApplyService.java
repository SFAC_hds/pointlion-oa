package com.ruoyi.back.oa.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.back.oa.mapper.OaTicketApplyMapper;
import com.ruoyi.back.oa.domain.OaTicketApply;
import com.ruoyi.back.oa.service.OaTicketApplyService;

/**
 * 车票申请Service业务层处理
 * 
 * @author pointLion
 * @date 2023-05-29
 */
@Service
public class OaTicketApplyService
{
    @Autowired
    private OaTicketApplyMapper oaTicketApplyMapper;

    /**
     * 查询车票申请
     * 
     * @param id 车票申请主键
     * @return 车票申请
     */
    public OaTicketApply selectOaTicketApplyById(Long id)
    {
        return oaTicketApplyMapper.selectOaTicketApplyById(id);
    }

    /**
     * 查询车票申请列表
     * 
     * @param oaTicketApply 车票申请
     * @return 车票申请
     */
    public List<OaTicketApply> selectOaTicketApplyList(OaTicketApply oaTicketApply)
    {
        return oaTicketApplyMapper.selectOaTicketApplyList(oaTicketApply);
    }

    /**
     * 新增车票申请
     * 
     * @param oaTicketApply 车票申请
     * @return 结果
     */
    public int insertOaTicketApply(OaTicketApply oaTicketApply)
    {
        oaTicketApply.setCreateTime(DateUtils.getNowDate());
        return oaTicketApplyMapper.insertOaTicketApply(oaTicketApply);
    }

    /**
     * 修改车票申请
     * 
     * @param oaTicketApply 车票申请
     * @return 结果
     */
    public int updateOaTicketApply(OaTicketApply oaTicketApply)
    {
        oaTicketApply.setUpdateTime(DateUtils.getNowDate());
        return oaTicketApplyMapper.updateOaTicketApply(oaTicketApply);
    }

    /**
     * 批量删除车票申请
     * 
     * @param ids 需要删除的车票申请主键
     * @return 结果
     */
    public int deleteOaTicketApplyByIds(Long[] ids)
    {
        return oaTicketApplyMapper.deleteOaTicketApplyByIds(ids);
    }

    /**
     * 删除车票申请信息
     * 
     * @param id 车票申请主键
     * @return 结果
     */
    public int deleteOaTicketApplyById(Long id)
    {
        return oaTicketApplyMapper.deleteOaTicketApplyById(id);
    }
}
