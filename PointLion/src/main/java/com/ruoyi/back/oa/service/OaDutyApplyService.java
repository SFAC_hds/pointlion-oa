package com.ruoyi.back.oa.service;

import cn.hutool.core.util.StrUtil;
import com.ruoyi.back.erp.service.SerialService;
import com.ruoyi.back.oa.domain.OaDutyApply;
import com.ruoyi.back.oa.domain.OaReimburseApply;
import com.ruoyi.back.oa.mapper.OaDutyApplyMapper;
import com.ruoyi.common.constant.CommonConstants;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.flowable.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 假勤申请Service业务层处理
 * 
 * @author pointLion
 * @date 2023-06-01
 */
@Service
public class OaDutyApplyService
{
    @Autowired
    private OaDutyApplyMapper oaDutyApplyMapper;
    @Autowired
    private SerialService serialService;
    @Autowired
    private FlowProcessInstanceService flowProcessInstanceService;

    /**
     * 查询假勤申请
     * 
     * @param id 假勤申请主键
     * @return 假勤申请
     */
    public OaDutyApply selectOaDutyApplyById(Long id)
    {
        return oaDutyApplyMapper.selectOaDutyApplyById(id);
    }

    /**
     * 查询假勤申请列表
     * 
     * @param oaDutyApply 假勤申请
     * @return 假勤申请
     */
    public List<OaDutyApply> selectOaDutyApplyList(OaDutyApply oaDutyApply)
    {
        return oaDutyApplyMapper.selectOaDutyApplyList(oaDutyApply);
    }

    /**
     * 新增假勤申请
     * 
     * @param oaDutyApply 假勤申请
     * @return 结果
     */
    public int insertOaDutyApply(OaDutyApply oaDutyApply)
    {
        oaDutyApply.setCreateTime(DateUtils.getNowDate());
        //生成订单编号
        String billCode = serialService.generateBillCodeByBillType(oaDutyApply.getBillType());
        oaDutyApply.setBillCode(billCode);
        oaDutyApply.setCreateBy(SecurityUtils.getLoginUser().getUsername());
        return oaDutyApplyMapper.insertOaDutyApply(oaDutyApply);
    }

    /**
     * 修改假勤申请
     * 
     * @param oaDutyApply 假勤申请
     * @return 结果
     */
    public int updateOaDutyApply(OaDutyApply oaDutyApply)
    {
        oaDutyApply.setUpdateTime(DateUtils.getNowDate());
        return oaDutyApplyMapper.updateOaDutyApply(oaDutyApply);
    }

    /**
     * 批量删除假勤申请
     * 
     * @param ids 需要删除的假勤申请主键
     * @return 结果
     */
    public int deleteOaDutyApplyByIds(Long[] ids)
    {
        return oaDutyApplyMapper.deleteOaDutyApplyByIds(ids);
    }

    /**
     * 删除假勤申请信息
     * 
     * @param id 假勤申请主键
     * @return 结果
     */
    public int deleteOaDutyApplyById(Long id)
    {
        return oaDutyApplyMapper.deleteOaDutyApplyById(id);
    }




    /****
     * 提交流程
     * @param id
     */
    @Transactional
    public void submitFlow(Long id){
        OaDutyApply apply = oaDutyApplyMapper.selectOaDutyApplyById(id);
        String billType = apply.getBillType();
        if(StrUtil.isBlank(billType)){
            throw new RuntimeException("单据类型为空，无法提交相应流程");
        }
        //单据类型对应流程编码
        String defKey = billType;
        //启动流程
        ProcessInstance procIns = flowProcessInstanceService.startProcess(id,apply.getReason(),billType);
        //更新单据状态
        OaDutyApply updateApply = new OaDutyApply();
        updateApply.setId(apply.getId());
        updateApply.setFlowInsId(procIns.getId());
        updateApply.setFlowKey(defKey);
        updateApply.setStatus(CommonConstants.FLOW_STATUS_SUBMIT);//更新状态
        updateApply.setUpdateTime(new Date());
        updateApply.setSubmitTime(new Date());
        oaDutyApplyMapper.updateOaDutyApply(updateApply);
    }





    /****
     * 取回流程
     * @param id
     */
    public void cancleFlow(Long id){
        OaDutyApply apply = oaDutyApplyMapper.selectOaDutyApplyById(id);
        String insId = apply.getFlowInsId();
        if(StrUtil.isNotBlank(insId)){
            flowProcessInstanceService.cancleProcesInstance(insId,"用户撤销");
        }
        OaDutyApply updateApply = new OaDutyApply();
        updateApply.setId(id);
        updateApply.setStatus("0");
        updateApply.setFlowInsId("");
        updateApply.setFlowKey("");
        updateApply.setUpdateTime(new Date());
        oaDutyApplyMapper.updateOaDutyApply(updateApply);
    }
}
