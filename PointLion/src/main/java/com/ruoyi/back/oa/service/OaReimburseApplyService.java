package com.ruoyi.back.oa.service;

import cn.hutool.core.util.StrUtil;
import com.ruoyi.back.erp.service.SerialService;
import com.ruoyi.back.oa.domain.OaReimburseApply;
import com.ruoyi.back.oa.domain.OaReimburseFeeitem;
import com.ruoyi.back.oa.mapper.OaReimburseApplyMapper;
import com.ruoyi.common.constant.BillTypeEnum;
import com.ruoyi.common.constant.CommonConstants;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 报销申请Service业务层处理
 * 
 * @author pointLion
 * @date 2023-05-29
 */
@Service
public class OaReimburseApplyService
{
    @Autowired
    private OaReimburseApplyMapper oaReimburseApplyMapper;
    @Autowired
    private SerialService serialService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private HistoryService historyService;
    @Autowired
    private FlowProcessInstanceService flowProcessInstanceService;


    /**
     * 查询报销申请
     * 
     * @param id 报销申请主键
     * @return 报销申请
     */
    public OaReimburseApply selectOaReimburseApplyById(Long id)
    {
        return oaReimburseApplyMapper.selectOaReimburseApplyById(id);
    }

    /**
     * 查询报销申请列表
     * 
     * @param oaReimburseApply 报销申请
     * @return 报销申请
     */
    public List<OaReimburseApply> selectOaReimburseApplyList(OaReimburseApply oaReimburseApply)
    {
        return oaReimburseApplyMapper.selectOaReimburseApplyList(oaReimburseApply);
    }

    /**
     * 新增报销申请
     * 
     * @param oaReimburseApply 报销申请
     * @return 结果
     */
    @Transactional
    public int insertOaReimburseApply(OaReimburseApply oaReimburseApply)
    {
        oaReimburseApply.setCreateTime(DateUtils.getNowDate());
        //生成订单编号
        String billCode = serialService.generateBillCodeByBillType(oaReimburseApply.getBillType());
        oaReimburseApply.setBillcode(billCode);
        oaReimburseApply.setCreateBy(SecurityUtils.getLoginUser().getUsername());
        int rows = oaReimburseApplyMapper.insertOaReimburseApply(oaReimburseApply);
        insertoaReimburseFeeitem(oaReimburseApply);
        return rows;
    }

    /**
     * 修改报销申请
     * 
     * @param oaReimburseApply 报销申请
     * @return 结果
     */
    @Transactional
    public int updateOaReimburseApply(OaReimburseApply oaReimburseApply)
    {
        oaReimburseApply.setUpdateTime(DateUtils.getNowDate());
        oaReimburseApplyMapper.deleteoaReimburseFeeitemByReimburseId(oaReimburseApply.getId());
        insertoaReimburseFeeitem(oaReimburseApply);
        return oaReimburseApplyMapper.updateOaReimburseApply(oaReimburseApply);
    }

    /**
     * 批量删除报销申请
     * 
     * @param ids 需要删除的报销申请主键
     * @return 结果
     */
    @Transactional
    public int deleteOaReimburseApplyByIds(Long[] ids)
    {
        oaReimburseApplyMapper.deleteoaReimburseFeeitemByReimburseIds(ids);
        return oaReimburseApplyMapper.deleteOaReimburseApplyByIds(ids);
    }

    /**
     * 删除报销申请信息
     * 
     * @param id 报销申请主键
     * @return 结果
     */
    @Transactional
    public int deleteOaReimburseApplyById(Long id)
    {
        oaReimburseApplyMapper.deleteoaReimburseFeeitemByReimburseId(id);
        return oaReimburseApplyMapper.deleteOaReimburseApplyById(id);
    }

    /**
     * 新增报销费用明细信息
     * 
     * @param oaReimburseApply 报销申请对象
     */
    public void insertoaReimburseFeeitem(OaReimburseApply oaReimburseApply)
    {
        List<OaReimburseFeeitem> oaReimburseFeeitemList = oaReimburseApply.getOaReimburseFeeitemList();
        Long id = oaReimburseApply.getId();
        if (StringUtils.isNotNull(oaReimburseFeeitemList))
        {
            List<OaReimburseFeeitem> list = new ArrayList<OaReimburseFeeitem>();
            for (OaReimburseFeeitem oaReimburseFeeitem : oaReimburseFeeitemList)
            {
                oaReimburseFeeitem.setReimburseId(id);
                list.add(oaReimburseFeeitem);
            }
            if (list.size() > 0)
            {
                oaReimburseApplyMapper.batchoaReimburseFeeitem(list);
            }
        }
    }




    /****
     * 提交流程
     * @param id
     */
    @Transactional
    public void submitFlow(Long id){
        OaReimburseApply apply = oaReimburseApplyMapper.selectOaReimburseApplyById(id);
        String billType = apply.getBillType();
        if(StrUtil.isBlank(billType)){
            throw new RuntimeException("单据类型为空，无法提交相应流程");
        }
        //单据类型对应流程编码
        String defKey = billType;
        //启动流程
        ProcessInstance procIns = flowProcessInstanceService.startProcess(id,apply.getDes(),billType);
        //更新单据状态
        OaReimburseApply updateApply = new OaReimburseApply();
        updateApply.setId(apply.getId());
        updateApply.setFlowInsId(procIns.getId());
        updateApply.setFlowKey(defKey);
        updateApply.setStatus(CommonConstants.FLOW_STATUS_SUBMIT);//更新状态
        updateApply.setUpdateTime(new Date());
        updateApply.setSubmitTime(new Date());
        oaReimburseApplyMapper.updateOaReimburseApply(updateApply);
    }





    /****
     * 取回流程
     * @param id
     */
    public void cancleFlow(Long id){
        OaReimburseApply apply = oaReimburseApplyMapper.selectOaReimburseApplyById(id);
        String insId = apply.getFlowInsId();
        if(StrUtil.isNotBlank(insId)){
            flowProcessInstanceService.cancleProcesInstance(insId,"用户撤销");
        }
        OaReimburseApply updateApply = new OaReimburseApply();
        updateApply.setId(id);
        updateApply.setStatus("0");
        updateApply.setFlowInsId("");
        updateApply.setFlowKey("");
        updateApply.setUpdateTime(new Date());
        oaReimburseApplyMapper.updateOaReimburseApply(updateApply);
    }
}
