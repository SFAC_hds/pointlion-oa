package com.ruoyi.back.oa.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.back.oa.mapper.OaMeetingroomApplyMapper;
import com.ruoyi.back.oa.domain.OaMeetingroomApply;
import com.ruoyi.back.oa.service.OaMeetingroomApplyService;

/**
 * 会议室申请Service业务层处理
 * 
 * @author pointLion
 * @date 2023-05-29
 */
@Service
public class OaMeetingroomApplyService
{
    @Autowired
    private OaMeetingroomApplyMapper oaMeetingroomApplyMapper;

    /**
     * 查询会议室申请
     * 
     * @param id 会议室申请主键
     * @return 会议室申请
     */
    public OaMeetingroomApply selectOaMeetingroomApplyById(Long id)
    {
        return oaMeetingroomApplyMapper.selectOaMeetingroomApplyById(id);
    }

    /**
     * 查询会议室申请列表
     * 
     * @param oaMeetingroomApply 会议室申请
     * @return 会议室申请
     */
    public List<OaMeetingroomApply> selectOaMeetingroomApplyList(OaMeetingroomApply oaMeetingroomApply)
    {
        return oaMeetingroomApplyMapper.selectOaMeetingroomApplyList(oaMeetingroomApply);
    }

    /**
     * 新增会议室申请
     * 
     * @param oaMeetingroomApply 会议室申请
     * @return 结果
     */
    public int insertOaMeetingroomApply(OaMeetingroomApply oaMeetingroomApply)
    {
        oaMeetingroomApply.setCreateTime(DateUtils.getNowDate());
        return oaMeetingroomApplyMapper.insertOaMeetingroomApply(oaMeetingroomApply);
    }

    /**
     * 修改会议室申请
     * 
     * @param oaMeetingroomApply 会议室申请
     * @return 结果
     */
    public int updateOaMeetingroomApply(OaMeetingroomApply oaMeetingroomApply)
    {
        oaMeetingroomApply.setUpdateTime(DateUtils.getNowDate());
        return oaMeetingroomApplyMapper.updateOaMeetingroomApply(oaMeetingroomApply);
    }

    /**
     * 批量删除会议室申请
     * 
     * @param ids 需要删除的会议室申请主键
     * @return 结果
     */
    public int deleteOaMeetingroomApplyByIds(Long[] ids)
    {
        return oaMeetingroomApplyMapper.deleteOaMeetingroomApplyByIds(ids);
    }

    /**
     * 删除会议室申请信息
     * 
     * @param id 会议室申请主键
     * @return 结果
     */
    public int deleteOaMeetingroomApplyById(Long id)
    {
        return oaMeetingroomApplyMapper.deleteOaMeetingroomApplyById(id);
    }
}
