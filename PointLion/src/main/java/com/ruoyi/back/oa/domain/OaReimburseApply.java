package com.ruoyi.back.oa.domain;

import lombok.Data;
import java.math.BigDecimal;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 报销申请对象 oa_reimburse_apply
 * 
 * @author pointLion
 * @date 2023-05-29
 */
@Data
public class OaReimburseApply extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 用户ID（单据 拥有者） */
    @Excel(name = "用户ID", readConverterExp = "单=据,拥=有者")
    private Long userId;

    /** 部门ID（单据拥有部门） */
    @Excel(name = "部门ID", readConverterExp = "单=据拥有部门")
    private Long deptId;

    /** 流程状态 */
    @Excel(name = "流程状态")
    private String status;

    /** 申请流程 */
    @Excel(name = "申请流程")
    private String flowKey;

    /** 流程实例id */
    private String flowInsId;

    /** 申请标题 */
    @Excel(name = "申请标题")
    private String applyTitle;

    /** 提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "提交时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date submitTime;

    /** 单据编号 */
    @Excel(name = "单据编号")
    private String billcode;

    /** 总金额 */
    @Excel(name = "总金额")
    private BigDecimal totalAmt;

    /** 单据类型 */
    @Excel(name = "单据类型")
    private String billType;

    /** 月度 */
    @Excel(name = "月度")
    private String month;

    /** 描述信息 */
    @Excel(name = "描述信息")
    private String des;

    /** 出差地点 */
    @Excel(name = "出差地点")
    private String city;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 票据总数 */
    @Excel(name = "票据总数")
    private Long billCount;

    /****
     * 票据快照
     */
    private String imgUrl;

    /*****
     * 补贴标准
     */
    private BigDecimal standardSubsidy;

    /****
     * 出差天数
     */
    private Long days;

    /****
     * 补贴总金额
     */
    private BigDecimal subsidyAmt;


    /****
     * 票据总金额
     */
    private BigDecimal billAmt;


    /** 报销费用明细信息 */
    private List<OaReimburseFeeitem> oaReimburseFeeitemList;



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("deptId", getDeptId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("status", getStatus())
            .append("flowKey", getFlowKey())
            .append("flowInsId", getFlowInsId())
            .append("applyTitle", getApplyTitle())
            .append("submitTime", getSubmitTime())
            .append("billcode", getBillcode())
            .append("remark", getRemark())
            .append("totalAmt", getTotalAmt())
            .append("billType", getBillType())
            .append("month", getMonth())
            .append("des", getDes())
            .append("city", getCity())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("billCount", getBillCount())
            .toString();
    }
}
