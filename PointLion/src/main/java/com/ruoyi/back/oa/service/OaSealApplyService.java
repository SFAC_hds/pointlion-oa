package com.ruoyi.back.oa.service;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.back.oa.mapper.OaSealApplyMapper;
import com.ruoyi.back.oa.domain.OaSealApply;
import com.ruoyi.back.oa.service.OaSealApplyService;

/**
 * 公章使用Service业务层处理
 * 
 * @author pointLion
 * @date 2023-05-29
 */
@Service
public class OaSealApplyService
{
    @Autowired
    private OaSealApplyMapper oaSealApplyMapper;

    /**
     * 查询公章使用
     * 
     * @param id 公章使用主键
     * @return 公章使用
     */
    public OaSealApply selectOaSealApplyById(Long id)
    {
        return oaSealApplyMapper.selectOaSealApplyById(id);
    }

    /**
     * 查询公章使用列表
     * 
     * @param oaSealApply 公章使用
     * @return 公章使用
     */
    public List<OaSealApply> selectOaSealApplyList(OaSealApply oaSealApply)
    {
        return oaSealApplyMapper.selectOaSealApplyList(oaSealApply);
    }

    /**
     * 新增公章使用
     * 
     * @param oaSealApply 公章使用
     * @return 结果
     */
    public int insertOaSealApply(OaSealApply oaSealApply)
    {
        oaSealApply.setCreateTime(DateUtils.getNowDate());
        return oaSealApplyMapper.insertOaSealApply(oaSealApply);
    }

    /**
     * 修改公章使用
     * 
     * @param oaSealApply 公章使用
     * @return 结果
     */
    public int updateOaSealApply(OaSealApply oaSealApply)
    {
        oaSealApply.setUpdateTime(DateUtils.getNowDate());
        return oaSealApplyMapper.updateOaSealApply(oaSealApply);
    }

    /**
     * 批量删除公章使用
     * 
     * @param ids 需要删除的公章使用主键
     * @return 结果
     */
    public int deleteOaSealApplyByIds(Long[] ids)
    {
        return oaSealApplyMapper.deleteOaSealApplyByIds(ids);
    }

    /**
     * 删除公章使用信息
     * 
     * @param id 公章使用主键
     * @return 结果
     */
    public int deleteOaSealApplyById(Long id)
    {
        return oaSealApplyMapper.deleteOaSealApplyById(id);
    }
}
